#include "FBW.h"
#include <sstream>
#include <map>
#include <algorithm>
#include <numeric>
#include <iostream>

const std::string FIZZ = "Fizz";
const std::string BUZZ = "Buzz";
const std::string WOOF = "Woof";

std::map<int, std::string> words = {{3, "Fizz"},
                                    {5, "Buzz"},
                                    {7, "Woof"}};
std::map<char, std::string> wordsContains = {{'3', "Fizz"},
                                             {'5', "Buzz"},
                                             {'7', "Woof"}};


Converter::Result::Result() : modified{false} {

}

Converter::Converter() :
        contains{std::make_unique<ContainsNoop>()},
        divisible{std::make_unique<DivisibleNoop>()} {

}

std::string Converter::convert(int number) {
    std::ostringstream oss;
    Result result;
    result = (*divisible)(number);
    result = (*contains)(result, number);

    return result.result;
}

ConverterBuilder &ConverterBuilder::setWithDivisible(bool withDivisible) {
    this->withDivisible = withDivisible;
    return *this;
}

ConverterBuilder &ConverterBuilder::setWithContains(bool withContains) {
    this->withContains = withContains;
    return *this;
}

ConverterBuilder &ConverterBuilder::setWithZeros(bool withZeros) {
    this->withZeros = withZeros;
    return *this;
}

Converter ConverterBuilder::build() {
    Converter c;
    if (withDivisible) {
        c.divisible = std::make_unique<Converter::DivisibleImpl>();
    } else {
        c.divisible = std::make_unique<Converter::DivisibleNoop>();
    }
    if (withContains && withZeros) {
        c.contains = std::make_unique<Converter::ContainsImplWithZero>();
    } else if (withContains && !withZeros) {
        c.contains = std::make_unique<Converter::ContainsImplNoZero>();
    } else {
        c.contains = std::make_unique<Converter::ContainsNoop>();
    }
    return c;
}

Converter::Result Converter::DivisibleNoop::operator()(int number) {
    Converter::Result result;
    result.result = std::to_string(number);
    return result;
}

Converter::Result Converter::DivisibleImpl::operator()(int number) {
    std::ostringstream oss;
    Converter::Result result;
    for (const auto &keyValuePair : words) {
        if (number % keyValuePair.first == 0) {
            result.modified = true;
            oss << keyValuePair.second;
        }
    }
    result.result = oss.str();
    return result;
}

Converter::Result Converter::ContainsNoop::operator()(Converter::Result oldResult, int number) {
    return oldResult;
}

Converter::Result Converter::ContainsImplNoZero::operator()(Converter::Result oldResult, int number) {
    Converter::Result result;
    std::ostringstream oss;
    std::string numString = std::to_string(number);
    for (auto digit : numString) {
        auto word = wordsContains.find(digit);
        if (word != wordsContains.end()) {
            result.modified = true;
            oss << word->second;
        }
    }
    if (oldResult.modified)
        result.result = oldResult.result + oss.str();
    else
        result.result = oss.str();
    return result;
}

Converter::Result Converter::ContainsImplWithZero::operator()(Converter::Result oldResult, int number) {
    Converter::Result result;
    std::ostringstream oss;
    std::string numString = std::to_string(number);
    for (auto digit : numString) {
        auto word = wordsContains.find(digit);
        if (word != wordsContains.end()) {
            result.modified = true;
            oss << word->second;
        }
        if (digit == '0') oss << '*';
    }
    if (oldResult.modified) {
        oldResult.result += oss.str();
        return oldResult;
    }
    if (result.modified) {
        return result;
    }

    std::replace_if(numString.begin(), numString.end(), [](char c) {
        return c == '0';
    }, '*');
    result.result = numString;
    result.modified = numString != std::to_string(number);

    return result;
}
