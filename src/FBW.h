//
// Created by Meritis Local on 08/10/2019.
//

#pragma once

#include <string>
#include <memory>

class Converter {
	friend class ConverterBuilder;

public:
    struct Result {
        std::string result;
        bool modified;
        Result();
    };

private:

    struct Divisible {
        virtual Result operator()(int number) = 0;
    };

    struct DivisibleNoop: Divisible {
        virtual Result operator()(int number) override ;
    };

    struct DivisibleImpl: Divisible {
        virtual Result operator()(int number) override ;
    };

    struct Contains {
        virtual Result operator()(Converter::Result oldResult, int number) = 0;
    };

    struct ContainsNoop: Contains {
        virtual Result operator()(Converter::Result oldResult, int number) override;
    };

    struct ContainsImplNoZero: Contains {
        Result operator()(Converter::Result oldResult, int number) override;
    };

    struct ContainsImplWithZero: Contains {
        Result operator()(Converter::Result oldResult, int number) override;
    };

public:
	std::string convert(int number);
	Converter();

private:
	std::unique_ptr<Divisible> divisible;
	std::unique_ptr<Contains> contains;
};

class ConverterBuilder
{
public:
	ConverterBuilder& setWithDivisible(bool withDivisible = true);
	ConverterBuilder& setWithContains(bool withContains = true);
	ConverterBuilder& setWithZeros(bool withZeros = true);
	Converter build();

private:
	bool withDivisible;
	bool withContains;
	bool withZeros;
};
