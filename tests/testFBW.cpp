#include <string>
#include "gtest/gtest.h"
#include "../src/FBW.h"

const std::string FIZZ = "Fizz";
const std::string BUZZ = "Buzz";
const std::string WOOF = "Woof";

TEST(FBW, fizzDiv)
{
	Converter c = ConverterBuilder().setWithDivisible(true).setWithContains(false).build();
    EXPECT_TRUE(c.convert(3).compare(FIZZ) == 0);
    EXPECT_TRUE(c.convert(6).compare(FIZZ) == 0);
    EXPECT_TRUE(c.convert(999).compare(FIZZ) == 0);
    //
    EXPECT_TRUE(c.convert(5).compare(BUZZ) == 0);
	EXPECT_TRUE(c.convert(55).compare(BUZZ) == 0);
	EXPECT_TRUE(c.convert(200).compare(BUZZ) == 0);
	//
	EXPECT_TRUE(c.convert(15).compare(FIZZ+BUZZ) == 0);
	//
	EXPECT_TRUE(c.convert(7).compare(WOOF) == 0);
	EXPECT_TRUE(c.convert(21).compare(FIZZ+WOOF) == 0);
	EXPECT_TRUE(c.convert(105).compare(FIZZ+BUZZ+WOOF) == 0);
}

TEST(FBW, fizzContains)
{
	Converter c = ConverterBuilder().setWithDivisible(false).setWithContains(true).build();
	EXPECT_TRUE(c.convert(3).compare(FIZZ) == 0);
	EXPECT_TRUE(c.convert(5).compare(BUZZ) == 0);
	EXPECT_TRUE(c.convert(7).compare(WOOF) == 0);
	EXPECT_TRUE(c.convert(3573).compare(FIZZ+BUZZ+WOOF+FIZZ) == 0);
}

TEST(FBW, FBWAll)
{
	Converter c = ConverterBuilder().setWithDivisible(true).setWithContains(true).setWithZeros(true).build();
	EXPECT_TRUE(c.convert(4).compare("4") == 0);
	EXPECT_TRUE(c.convert(2535).compare("FizzBuzzBuzzFizzBuzz") == 0);
	EXPECT_TRUE(c.convert(35).compare("BuzzWoofFizzBuzz") == 0);
    EXPECT_TRUE(c.convert(0).compare(FIZZ+BUZZ+WOOF+"*") == 0);
    EXPECT_TRUE(c.convert(101).compare("1*1") == 0);
    EXPECT_TRUE(c.convert(30).compare("FizzBuzzFizz*") == 0);
    EXPECT_TRUE(c.convert(3105).compare("FizzBuzzFizz*Buzz") == 0);
}